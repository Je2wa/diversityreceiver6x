# diversityreceiver6x

Acknowledgements:

The electronic circuit is based on rx5808-pro-diversity-develop https://github.com/sheaivey/rx5808-pro-diversity

The micro python program uses the basic functions from rpi-rx5808-stream-master https://github.com/xythobuz/rpi-rx5808-stream

Thanks for both projects!

diversityreceiver6x is licensed under the **GNU** General **Public** License.



## FPV receiver for 360 degrees



![FPVimGarten](picture/FPVimGarten.jpg)



With simple directional antennas, a six-way receiver can capture an angle of 360. This program uses an ESP32 with MicroPython. 

The rx5808 are controlled by SPI interface. Two interfaces are used. This way the ESP32 can control the rx5808 more reliably.

All rx5808 are switched by MOS switches. The power supply of the circuit is important. The whole circuit needs about 1.2 A. The Mos switches must be powered with 5V! 

My test setup uses central ground and 5 volt points.

The case is published on Thingiverse. [3D printed case](https://www.thingiverse.com/thing:4837182)

## Program flow

The program tests in the first step (sucheBesteFrequents) all 5.8 GHz channels. The 6 receiver is always used. The FPV transmitter must be near the receiver.

If a transmitter is found, the program starts the Diversity Timmer (diversity) which searches for the strongest receiver every 50ms and unlocks it.

The version:

fpvDivers58_2_LCD -> uses a LCD output to display the received channel and the used receiver (lcd_page).

fpvDivers58_2 -> uses a WLAN 2.4 Ghz hotspot - fpvDivers58 no password - and displays the found frequency and the used receiver in a web page.



## Hardware structure

The 6 rx5808 have a SPI input and did not need to be adapted. The ESP32 ports switch 3 receivers each.  So 6 ports are used. The power supply ( 5V ) is done by a USB battery. This needs 2A output power. The circuit needs about 1.2 A. For this current the setup needs a central point for the power supply. This is especially important to avoid attenuation of the video signal by the CMOS switches.

The circuit  - fpvDivers58-6x.pdf - and the setup is tested in a "POC" and works.



[For the LCD Display](https://www.thingiverse.com/thing:3207325)

[3D printed case](https://www.thingiverse.com/thing:4837182)

![P3213583](picture/P3213583.JPG)



Translated with www.DeepL.com/Translator (free version)



### German:



# diversityreceiver6x

Danksagung:

Die elektronische Schaltung basiert auf rx5808-pro-diversity-develop https://github.com/sheaivey/rx5808-pro-diversity

Das Micro-Python Programm verwendet die Basis Funktionen aus rpi-rx5808-stream-master https://github.com/xythobuz/rpi-rx5808-stream

Vielen Dank für die beiden Projekte!

diversityreceiver6x steht unter der **GNU** General **Public** License.



## FPV Empfänger für 360 Grad

Mit einfachen Richtfunk-Antennen kann ein sechsfach Empfänger einen Winkel von 360 erfassen. Dieses Programm verwendet einen ESP32 mit MicroPython. 

Die rx5808 werden mittels SPI-Schnittstelle angesteuert. Dabei werden zwei Schnittstellen verwendet. Der ESP32 kann so zuverlässiger die rx5808 ansteuern.

Alle rx5808 werden mittels MOS-Schaltern umgeschaltet. Dabei ist die Stromversorgung der Schaltung wichtig. Die gesamte Schaltung benötigt ca. 1.2 A. Die Mos-Schalter müssen mit 5V betrieben werden! 

Mein Testaufbau benutzt Zentrale Masse und 5 Volt Punkte.

Das Gehäuse ist auf Thingiverse veröffentlicht. [3D gedrucktes Gehäuse](https://www.thingiverse.com/thing:4837182)

## Programmablauf

Das Programm testet im ersten Schritt (sucheBesteFrequents) alle 5.8 GHz Kanäle. Dabei wird immer der 6 Empfänger verwendet. Der FPV-Sender muss sich in der nähe des Empfängers befinden.

Wird ein Sender gefunden startet das Programm den Diversity Timmer (diversity) der alle 50ms den stärksten Empfänger sucht und freischaltet.

Die Version:

fpvDivers58_2_LCD -> verwendet eine LCD Ausgabe für die Anzeige des Empfangenen Kanals und den verwenden Empfänger (lcd_page).

fpvDivers58_2 -> verwendet einen WLAN 2.4 Ghz Hotspot - fpvDivers58 kein Passwort - auf und gibt in einer Webseite die gefundene Frequenz und den verwendet Empfänger aus.



## Hardwareaufbau

Die 6 rx5808 besitzen einen SPI Eingang und mussten nicht angepasst werden. Die ESP32 Ports schalten jeweils 3 Empfänger.  Es werden also 6 Ports verwendet. Die Stromversorgung ( 5V ) wird durch einen USB-Akku durchgeführt. Dieser benötigt 2A Ausgangsleistung. Die Schaltung benötigt etwa 1.2 A. Für diesen Strom benötigt der Aufbau einen zentralen Punkte für die Stromversorgung. Dies ist besonders wichtig um Dämpfungen des Videosignals durch die CMOS-Schalter zu verweiden.

Die Schaltung - fpvDivers58-6x.pdf -  und der Aufbau ist in einem "POC" getestet und funktioniert.

[Für das LCD Display](https://www.thingiverse.com/thing:3207325)

[3D gedrucktes Gehäuse](https://www.thingiverse.com/thing:4837182)

