# fpcDivers58 is a Diversitiy Reciver with rx5808 and esp32
#
# Complete project details at https://RandomNerdTutorials.com
#
import machine
from machine import Pin, SoftI2C
from machine import ADC
import time
from machine import Timer
from mp_i2c_lcd1602 import LCD1602

#machine.freq(240000000)
#runstate startet die Diversity funktion
runstat = 0
#Rssi für web Frontend
globalRssi = 0
#Content für Webfrontend
content = ""
#
globalFrequents = ""
#
RXnummer = -1
# Empfänger unter badRssi werden nicht berücksichtigt
badRsss = 1000
#Start Empfänger für das suchen der besten Frequentz 0-5
startEmpfaenger = 5
#Hintergrund Prozess

tim1 = Timer(1)
tim1.init(period=8000, mode=Timer.PERIODIC, callback=lambda t:sucheBesteFrequents())
#auswahl Port für mehrer RX5808
pins = [Pin(13,Pin.OUT),Pin(10,Pin.OUT),Pin(23,Pin.OUT),Pin(25,Pin.OUT),Pin(19,Pin.OUT),Pin(18,Pin.OUT)]

# -----------------------------------------------------------------------------
# ----- RX5808 SPI GPIO interface -----
# esp32ttgo gpio Ports 21,16 and 17 RX 0-2
pin_data = Pin(21,Pin.OUT)
pin_ss = Pin(17,Pin.OUT)
pin_clock = Pin(16,Pin.OUT)
# esp32ttgo gpio Ports 27,4 and 2  RX 3-5
pin_data1 = Pin(02,Pin.OUT)
pin_ss1 = Pin(04,Pin.OUT)
pin_clock1 = Pin(27,Pin.OUT)

#RSSI Ports
rssi = [ ADC(Pin(35)),ADC(Pin(33)),ADC(Pin(34)),ADC(Pin(39)),ADC(Pin(32)),ADC(Pin(36))]


def getRssi(chanel):
    last_rssi = 0
    for j in range(80):
        rssi[chanel].atten(ADC.ATTN_11DB) 
        last_rssi = last_rssi + rssi[chanel].read()
           #time.sleep(0.05)
    last_rssi = last_rssi / 80
    return last_rssi


channel_values = [
    # Channel 1 - 8
    0x281D, 0x288F, 0x2902, 0x2914, 0x2987, 0x2999, 0x2A0C, 0x2A1E, # Raceband
    0x2A05, 0x299B, 0x2991, 0x2987, 0x291D, 0x2913, 0x2909, 0x289F, # Band A
    0x2903, 0x290C, 0x2916, 0x291F, 0x2989, 0x2992, 0x299C, 0x2A05, # Band B
    0x2895, 0x288B, 0x2881, 0x2817, 0x2A0F, 0x2A19, 0x2A83, 0x2A8D, # Band E
    0x2906, 0x2910, 0x291A, 0x2984, 0x298E, 0x2998, 0x2A02, 0x2A0C, # Band F / Airwave
    0x2609, 0x261C, 0x268E, 0x2701, 0x2713, 0x2786, 0x2798, 0x280B # Band D / 5.3
]

channel_frequencies = [
    # Channel 1 - 8
    5658, 5695, 5732, 5769, 5806, 5843, 5880, 5917, # Raceband
    5865, 5845, 5825, 5805, 5785, 5765, 5745, 5725, # Band A
    5733, 5752, 5771, 5790, 5809, 5828, 5847, 5866, # Band B
    5705, 5685, 5665, 5645, 5885, 5905, 5925, 5945, # Band E
    5740, 5760, 5780, 5800, 5820, 5840, 5860, 5880, # Band F / Airwave
    5362, 5399, 5436, 5473, 5510, 5547, 5584, 5621 # Band D / 5.3
]
#-------------------------------------------------------------------------------------------------------------------
#Basis Function A
def spi_sendbit_1_A():
    pin_clock.off()
    time.sleep(0.000001)

    pin_data.on()
    time.sleep(0.000001)
    pin_clock.on()
    time.sleep(0.000001)

    pin_clock.off()
    time.sleep(0.000001)

def spi_sendbit_0_A():
    pin_clock.off()
    time.sleep(0.000001)

    pin_data.off()
    time.sleep(0.000001)
    pin_clock.on()
    time.sleep(0.000001)

    pin_clock.off()
    time.sleep(0.000001)

def spi_readbit_A():
    pin_clock.off()
    time.sleep(0.000001)

    pin_clock.on()
    time.sleep(0.000001)

    
    #Readin bit from pin_data
    pin_data = Pin(21, Pin.OUT) 
    if pin_data.value == 1:
        return True
    else:
        return False
    
    #Put bit to pin_data
    pin_data = Pin(21,Pin.OUT)
    pin_clock.off()
    time.sleep(0.000001)

def spi_select_low_A():
    time.sleep(0.000001)
    pin_ss.off()
    time.sleep(0.000001)

def spi_select_high_A():
    time.sleep(0.000001)
    pin_ss.on()
    time.sleep(0.000001)

def get_register_A(reg):
    spi_select_high_A();
    time.sleep(0.000001)
    spi_select_low_A();

    for i in range(4):
        if reg & (1 << i):
            spi_sendbit_1_A();
        else:
            spi_sendbit_0_A();

    # Read from register
    spi_sendbit_0_A();

    GPIO.setup(pin_data, GPIO.IN)

    data = 0
    for i in range(20):
        # Is bit high or low?
        val = spi_readbit()
        if val:
            data |= (1 << i)

    # Finished clocking data in
    spi_select_high_A()
    time.sleep(0.000001)

    GPIO.setup(pin_data, GPIO.OUT)

    pin_ss.off()
    pin_clock.off()
    pin_data.off()

    return data

def get_frequencyA():
    channel_data = get_register_A(0x01)

    channel_freq = None
    for i in range(len(channel_values)):
        if channel_values[i] == channel_data:
            channel_freq = channel_frequencies[i]
            break

    if channel_freq == None:
        return "Unknown ({})".format(hex(channel_data))
    else:
        return str(channel_freq) + "MHz"

def get_osc_settings_A():
    val = get_register_A(0x08)
    pre = get_register_A(0x00)
    return "(Settings: {}; Reference: {}MHz)".format(hex(val), str(pre))

def set_register_A(reg, val):
    spi_select_high_A();
    time.sleep(0.000001)
    spi_select_low_A();

    for i in range(4):
        if reg & (1 << i):
            spi_sendbit_1_A();
        else:
            spi_sendbit_0_A();

    # Write to register
    spi_sendbit_1_A();

    # D0-D15
    for i in range(20):
        # Is bit high or low?
        if val & 0x1:
            spi_sendbit_1_A()
        else:
            spi_sendbit_0_A()

        # Shift bits along to check the next one
        val >>= 1

    # Finished clocking data in
    spi_select_high_A()
    time.sleep(0.000001)
    spi_select_low_A();


#-------------------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------------------
#Basis Function B
def spi_sendbit_1_B():
    pin_clock1.off()
    time.sleep(0.000001)

    pin_data1.on()
    time.sleep(0.000001)
    pin_clock1.on()
    time.sleep(0.000001)

    pin_clock1.off()
    time.sleep(0.000001)

def spi_sendbit_0_B():
    pin_clock1.off()
    time.sleep(0.000001)

    pin_data1.off()
    time.sleep(0.000001)
    pin_clock1.on()
    time.sleep(0.000001)

    pin_clock1.off()
    time.sleep(0.000001)

def spi_readbit_B():
    pin_clock1.off()
    time.sleep(0.000001)

    pin_clock1.on()
    time.sleep(0.000001)
    pin_data = Pin(02,Pin.OUT)
    #Readin bit from pin_data
    
    if pin_data1.value == 1:
        return True
    else:
        return False
    
    #Put bit to pin_data
    pin_data = Pin(02,Pin.OUT)
    pin_clock1.off()
    time.sleep(0.000001)

def spi_select_low_B():
    time.sleep(0.000001)
    pin_ss1.off()
    time.sleep(0.000001)

def spi_select_high_B():
    time.sleep(0.000001)
    pin_ss1.on()
    time.sleep(0.000001)

def get_register_B(reg):
    spi_select_high_B();
    time.sleep(0.000001)
    spi_select_low_B();

    for i in range(4):
        if reg & (1 << i):
            spi_sendbit_1_B();
        else:
            spi_sendbit_0_B();

    # Read from register
    spi_sendbit_0_B();

    GPIO.setup(pin_data1, GPIO.IN)

    data = 0
    for i in range(20):
        # Is bit high or low?
        val = spi_readbit_B()
        if val:
            data |= (1 << i)

    # Finished clocking data in
    spi_select_high_B()
    time.sleep(0.000001)

    GPIO.setup(pin_data1, GPIO.OUT)

    pin_ss1.off()
    pin_clock1.off()
    pin_data1.off()

    return data

def get_frequency_B():
    channel_data = get_register_B(0x01)

    channel_freq = None
    for i in range(len(channel_values)):
        if channel_values[i] == channel_data:
            channel_freq = channel_frequencies[i]
            break

    if channel_freq == None:
        return "Unknown ({})".format(hex(channel_data))
    else:
        return str(channel_freq) + "MHz"

def get_osc_settings_B():
    val = get_register_B(0x08)
    pre = get_register_B(0x00)
    return "(Settings: {}; Reference: {}MHz)".format(hex(val), str(pre))

def set_register_B(reg, val):
    spi_select_high_B();
    time.sleep(0.000001)
    spi_select_low_B();

    for i in range(4):
        if reg & (1 << i):
            spi_sendbit_1_B();
        else:
            spi_sendbit_0_B();

    # Write to register
    spi_sendbit_1_B();

    # D0-D15
    for i in range(20):
        # Is bit high or low?
        if val & 0x1:
            spi_sendbit_1_B()
        else:
            spi_sendbit_0_B()

        # Shift bits along to check the next one
        val >>= 1

    # Finished clocking data in
    spi_select_high_B()
    time.sleep(0.000001)
    spi_select_low_B();

def set_frequency(freq):
    channel_data = None
    for i in range(len(channel_frequencies)):
        if str(channel_frequencies[i]) == freq:
            channel_data = channel_values[i]
            break

    if channel_data == None:
        s = "Error: unknown frequency {}MHz!".format(freq)
        print(s)
        return s

 #   print("Selected frequency: {}MHz ({})...".format(freq, channel_data))

    #set_register(0x08, 0x00)
    set_register_B(0x08, 0x03F40) # default values

    set_register_B(0x01, channel_data)
    
    set_register_A(0x08, 0x03F40) # default values

    set_register_A(0x01, channel_data)

    pin_ss1.off()
    pin_clock1.off()
    pin_data1.off()
    pin_ss.off()
    pin_clock.off()
    pin_data.off()

    return "Success (set freq to {})!".format(hex(channel_data))

#-------------------------------------------------------------------------------------------------------------------


# Sucht die Beste RSSI Frequentz als Frequentz es werden nur Singnale über 230 wargenommen
def get_best_frequency():
    global badRsss
    global startEmpfaenger
    
    channel_freq = None
    rssi = 0
    best_rssi = 0
    best_chanel = ""
    
    for i in range(len(channel_frequencies)):
       channel_freq = str(channel_frequencies[i])
       #Der 5 Empfänger gehört zur B Reihe 3-5
       frequents = set_frequency(channel_freq)
       
       time.sleep(0.08)
       
       #Berechne Mittelwert
       # Suche die beste Frequentz auf Port 3
       pins[startEmpfaenger].on()
       rssi = getRssi(startEmpfaenger)
     #  print("Rssi:" + str(rssi) + " Freq:" + str(channel_freq))
       if best_rssi < rssi:
           best_rssi = rssi
           best_chanel = channel_freq
           
     #  print("Best_Chanel;" + best_chanel)
     #  print("BEst RSSI:" + str(best_rssi))
 # es werden nur ergebnisse über 230 berücksichtigt
    if best_rssi < badRsss:
        best_chanel = ""
    return best_chanel
###################################
# Timer Funktionen
# Timer 1
#Suche Initial Kanal
# Belegt die Globalen Variablen conten für die Website und globalFrequents
#
def sucheBesteFrequents():
    
    global globalFrequents
    global content
    global LCD
    if globalFrequents == "":
        globalFrequents = get_best_frequency()
        if globalFrequents != "":
            frequents = set_frequency(globalFrequents)
            #Lcd Ausgabe
            LCD.puts(globalFrequents + "Mhz RS:",0,0)
            
            #Starte Div Timer
            tim0 = Timer(0)
            tim0.init(period=50, mode=Timer.PERIODIC, callback=lambda t:diversity())
            tim1.deinit()
        
    
# Vergleich Vorgang
# Timer 0
# Steuert die RX umsachaltung und ermittelt den Besten RX Empfänger
#
# runstat stuert die Syncronisation mit dem Webserver erst wenn dieser gestartet ist werden RSSI werte
# ermittelt. Dies Reduziert die CPU last
#
def diversity():
    global runstat
    global globalRssi
    global rssi
    global RXnummer
    rssi_last = 0
    best_rssi = 0
    bestRX = 0
    if runstat == 1:
        #print("Rssi1:" + str(getRssi(0)) + "Rssi2:" + str(getRssi(1)) + "Rssi3:" + str(getRssi(2)) + "Rssi4:" + str(getRssi(3)) + "Rssi5:" + str(getRssi(4)) + "Rssi6:" + str(getRssi(5)))
        for i in range(len(rssi)):
            rssi_last = getRssi(i)
            
            if best_rssi < rssi_last:
                best_rssi = rssi_last
                bestRX = i
        globalRssi = best_rssi
        #Der erste RX begint mit 1
        if (RXnummer != (bestRX + 1)):
            pins[bestRX].on
            pins[RXnummer -1].off
            RXnummer = bestRX + 1
            
         #   print("Neuer Kanal gefunden:" + str(RXnummer) + " RSSI:" + str(best_rssi))

################################### LCD Page ###################################################
#

def lcd_page(rssi,RXnummer):
    global LCD
    global globalFrequents
    
    LCD.puts(str(rssi),12,0)
    if (RXnummer == -1 ):
        LCD.puts("Empf. 6",0,1)
    else:
        rss = str(RXnummer)
        
        LCD.puts(rss,6,1)
  

#########################################################################################
# Main
########################################################



try:
  import usocket as socket
except:
  import socket

import network

import esp
esp.osdebug(None)

import gc

i2c = SoftI2C(scl=Pin(22), sda=Pin(0), freq=100000)

LCD = LCD1602(i2c,39)
LCD.clear()
LCD.puts("Starte V.1",0,0)

gc.collect()

#Close all Switches
pins[0].off
pins[1].off
pins[2].off
pins[3].off
pins[4].off
pins[5].off


runstat = 1
#LCD Ausgabe, Variablen werden durch Timer gefült
while True:
  time.sleep(0.5) 
  lcd_page(globalRssi,RXnummer)
  

